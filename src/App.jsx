import { useState } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(1)
  function multiply(){
    setCount(count*5)
  }
  function reset(){
    setCount(1)
  }
  

  return (
    <>
      <header>Multiply by 5</header>
      <main>
        <h1>{count}</h1>
        <button onClick={multiply}>x5</button>
        <button onClick={reset}>Reset</button>
      </main>
    </>
  )
}

export default App
